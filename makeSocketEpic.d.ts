/// <reference types="socket.io-client" />
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';
import { Epic } from 'redux-observable';
import { YapAction } from 'yapreact/utils/createAction';
export interface CUSocketMessage {
    statusCode: number;
    type: string;
    data?: {
        server?: any;
        client?: any;
    };
}
export interface StoreLike {
    getState: () => any;
}
export declare type SocketmanActionMapper = (message: CUSocketMessage) => YapAction<any>;
export interface SocketMakerOptions {
    socketUrl: string;
    startActionType: string | [string];
    stopActionType: string | [string];
    onInit?: (action: YapAction<any>, s: StoreLike, socket: SocketIOClient.Socket) => void;
    actionMapper?: SocketmanActionMapper;
}
export declare let socket: SocketIOClient.Socket;
declare const _default: ({ socketUrl, startActionType, stopActionType, onInit, actionMapper }: SocketMakerOptions) => Epic<YapAction<any>, any, any, YapAction<any>>;
export default _default;
