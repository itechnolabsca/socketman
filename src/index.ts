import { Epic } from 'redux-observable';
import { AnyAction } from 'redux';
import { Observable } from 'rxjs/Observable';

import makeSocketEpic, { socket, SocketMakerOptions } from './makeSocketEpic';
import logger from 'yapreact/utils/createLogger';
import { YapAction } from 'yapreact/utils/createAction';

const debug = logger('socketman:Socketman');

interface SendSocketMessage {
  data: any;
  auth?: string;
  type: string;
}

export interface SocketmanInput extends SocketMakerOptions {
  accessToken?: string;
}

/**
 * Socketman provides abstraction to deal with CU app sockets. Consumer gets to deal only with redux actions.
 *
 * Usage:
 * - Add socketman.epic in the epics chain. This is how Socketman will emit actions into the redux store.
 * - Use socketman.emit to emit new messages over the socket
 */
class Socketman {
  epic: Epic<YapAction<any>, any>;
  accessToken: string;

  constructor({
    socketUrl,
    startActionType,
    stopActionType,
    onInit,
    actionMapper,
    accessToken
  }: SocketmanInput) {
    this.epic = makeSocketEpic({
      socketUrl,
      startActionType: startActionType || 'LOGIN_USER',
      stopActionType: stopActionType || 'LOGOUT_USER',
      onInit,
      actionMapper
    });

    this.accessToken = accessToken;
  }

  emitRaw(type: string, message?: any) {
    if (!socket) {
      console.error('Socket connection has not been initiated.');
      return;
    }

    debug('Emitting: ', type, message, socket.id);
    socket.emit(type, message);
  }

  emit(type: string, data?: any, isAnonymous?: boolean) {
    const message: SendSocketMessage = {
      type,
      data
    };

    if (!isAnonymous && this.accessToken) {
      message.auth = this.accessToken;
    }

    this.emitRaw('new_message', message);
  }
}

export default Socketman;
