import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';
import { Action } from 'redux';
import { Epic } from 'redux-observable';
import * as io from 'socket.io-client';
import { YapAction } from 'yapreact/utils/createAction';
import logger from 'yapreact/utils/createLogger';
import { Socket } from 'socket.io-client';

const debug = logger('socketman:makeSocketEpic');

export interface CUSocketMessage {
  statusCode: number;
  type: string;
  data?: {
    server?: any;
    client?: any;
  };
}
interface WrappedCUSocketMessage {
  message: CUSocketMessage;
}

const createSocketObservable = (
  soc: SocketIOClient.Socket
): Observable<WrappedCUSocketMessage | CUSocketMessage> =>
  new Observable((observer) => {
    soc.on('message', (message: WrappedCUSocketMessage | CUSocketMessage) =>
      observer.next(message)
    );
    soc.on('error', (message: WrappedCUSocketMessage | CUSocketMessage) => observer.error(message));

    return () => {
      soc.disconnect();
    };
  });

const makeAction = (type: string) => (m: CUSocketMessage): YapAction<any> => {
  debug('Receiving', m);

  if (m && m.statusCode !== 200) {
    return { type: `@@socketman/failure/${type}`, payload: m };
  }

  if (type === 'UNKNOWN_EVENT') {
    return { type: '@@socketman/UNKNOWN_EVENT', payload: m };
  }

  // `message` data usually has {client,server} keys, but sometimes it is simply an object,
  // or even a string. Which is stupid.
  let data = m.data && m.data.server ? m.data.server : m.data;
  if (m.type !== 'connection' && data && typeof data === 'string') {
    try {
      data = JSON.parse(data);
    } catch (err) {
      /* eslint-disable no-console */
      debug('Was trying to parse: ', data);
      debug('Error while parsing incoming socket message', err);
      /* eslint-enable */
      data = {};
    }
  }

  return { type: `@@socketman/${type}`, payload: data };
};

const socketActions = {
  newMessage: makeAction('NEW_MESSAGE'),
  newAnonymousMessage: makeAction('NEW_ANONYMOUS_MESSAGE'),
  newAnonymousMessageReply: makeAction('NEW_ANONYMOUS_MESSAGE_REPLY'),
  onlineStatus: makeAction('ONLINE_STATUS'),
  pastMessages: makeAction('PAST_MESSAGES'),
  ackSentMessage: makeAction('ACK_SENT_MESSAGE'),
  login: makeAction('LOGIN'),
  updateMerchant: makeAction('UPDATE_MERCHANT'),
  unknown: makeAction('UNKNOWN_EVENT'),
  connected: makeAction('CONNECTED'),
  failedToConnect: makeAction('FAILED_TO_CONNECT'),
  getAnonymousMessages: makeAction('ANONYMOUS_MESSAGES'),
  getAnonymousMessagesV1: makeAction('ANONYMOUS_MESSAGES_V1'),
  getAnonymousMessage: makeAction('ANONYMOUS_MESSAGE'),
  getAnonymousMessageV1: makeAction('ANONYMOUS_MESSAGE_V1'),
  getAnonymousMessageReplies: makeAction('ANONYMOUS_MESSAGE_REPLIES'),
  getAnonymousMessageRepliesV1: makeAction('ANONYMOUS_MESSAGE_REPLIES_V1'),
  sendAnonymousMessageRepliesAck: makeAction('ACK_ANONYMOUS_MESSAGE_REPLY'),
  getAnonymousMessagesDepartment: makeAction('ANONYMOUS_MESSAGES_DEPARTMENT'),
  qrcode: makeAction('WEB_LOGIN_QRCODE'),
  qrsuccess: makeAction('WEB_LOGIN_SUCCESS')
};

const mapSocketToActions = (message: CUSocketMessage): YapAction<any> => {
  switch (message.type) {
    case 'connection':
      if (message.statusCode !== 200) {
        return socketActions.failedToConnect(message);
      }
      return socketActions.connected(message);
    case 'getWebMessages':
      return socketActions.pastMessages(message);
    case 'newMessageAck':
    case 'changeMessageStatus_1':
      return socketActions.ackSentMessage(message);
    default:
      return (socketActions[message.type] || socketActions.unknown)(message);
  }
};

export interface StoreLike {
  getState: () => any;
}

export type SocketmanActionMapper = (message: CUSocketMessage) => YapAction<any>;

export interface SocketMakerOptions {
  socketUrl: string;
  startActionType: string | [string];
  stopActionType: string | [string];
  onInit?: (action: YapAction<any>, s: StoreLike, socket: SocketIOClient.Socket) => void;
  actionMapper?: SocketmanActionMapper;
}

const filterAction = (action: Action, actionType: string | [string]): boolean => {
  if (Array.isArray(actionType)) {
    return actionType.indexOf(action.type) >= 0;
  }

  return action.type === actionType;
};

export let socket: SocketIOClient.Socket;
export default ({
  socketUrl,
  startActionType,
  stopActionType,
  onInit,
  actionMapper
}: SocketMakerOptions): Epic<YapAction<any>, any> => (action$, storeLike) =>
  action$
    .filter((action) => (!socket || !socket.connected) && filterAction(action, startActionType))
    .mergeMap((action) => {
      socket = io(socketUrl);

      if (onInit && typeof onInit === 'function') {
        onInit(action, storeLike, socket);
      }

      return createSocketObservable(socket).takeUntil(
        action$.filter((ac) => filterAction(ac, stopActionType))
      );
    })
    .map(
      (m): CUSocketMessage =>
        (m as WrappedCUSocketMessage).message
          ? (m as WrappedCUSocketMessage).message
          : (m as CUSocketMessage)
    )
    .map((m) => (actionMapper || ((): null => null))(m) || mapSocketToActions(m));
