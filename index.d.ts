import { Epic } from 'redux-observable';
import { SocketMakerOptions } from './makeSocketEpic';
import { YapAction } from 'yapreact/utils/createAction';
export interface SocketmanInput extends SocketMakerOptions {
    accessToken?: string;
}
/**
 * Socketman provides abstraction to deal with CU app sockets. Consumer gets to deal only with redux actions.
 *
 * Usage:
 * - Add socketman.epic in the epics chain. This is how Socketman will emit actions into the redux store.
 * - Use socketman.emit to emit new messages over the socket
 */
declare class Socketman {
    epic: Epic<YapAction<any>, any>;
    accessToken: string;
    constructor({socketUrl, startActionType, stopActionType, onInit, actionMapper, accessToken}: SocketmanInput);
    emitRaw(type: string, message?: any): void;
    emit(type: string, data?: any, isAnonymous?: boolean): void;
}
export default Socketman;
