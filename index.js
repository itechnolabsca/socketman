"use strict";
exports.__esModule = true;
var makeSocketEpic_1 = require("./makeSocketEpic");
var createLogger_1 = require("yapreact/utils/createLogger");
var debug = createLogger_1["default"]('socketman:Socketman');
/**
 * Socketman provides abstraction to deal with CU app sockets. Consumer gets to deal only with redux actions.
 *
 * Usage:
 * - Add socketman.epic in the epics chain. This is how Socketman will emit actions into the redux store.
 * - Use socketman.emit to emit new messages over the socket
 */
var Socketman = /** @class */ (function () {
    function Socketman(_a) {
        var socketUrl = _a.socketUrl, startActionType = _a.startActionType, stopActionType = _a.stopActionType, onInit = _a.onInit, actionMapper = _a.actionMapper, accessToken = _a.accessToken;
        this.epic = makeSocketEpic_1["default"]({
            socketUrl: socketUrl,
            startActionType: startActionType || 'LOGIN_USER',
            stopActionType: stopActionType || 'LOGOUT_USER',
            onInit: onInit,
            actionMapper: actionMapper
        });
        this.accessToken = accessToken;
    }
    Socketman.prototype.emitRaw = function (type, message) {
        if (!makeSocketEpic_1.socket) {
            console.error('Socket connection has not been initiated.');
            return;
        }
        debug('Emitting: ', type, message, makeSocketEpic_1.socket.id);
        makeSocketEpic_1.socket.emit(type, message);
    };
    Socketman.prototype.emit = function (type, data, isAnonymous) {
        var message = {
            type: type,
            data: data
        };
        if (!isAnonymous && this.accessToken) {
            message.auth = this.accessToken;
        }
        this.emitRaw('new_message', message);
    };
    return Socketman;
}());
exports["default"] = Socketman;
//# sourceMappingURL=index.js.map