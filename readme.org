* Socketman

- Common module for interfacing with CU app sockets from frontend
- Socketman is a redux module which provide a common interface for interacting with CU sockets


*It handles:*

- Connecting with the CU sockets including maintaining and closing the socket
- Handling incoming messages and dispatching appropriate redux actions
- Sending messages over the socket


*Consumer has to:*

- Handle dispatched messages by Socketman in redux reducers to change app state
