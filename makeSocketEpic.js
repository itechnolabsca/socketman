"use strict";
exports.__esModule = true;
var Observable_1 = require("rxjs/Observable");
require("rxjs/add/operator/takeUntil");
require("rxjs/add/operator/filter");
require("rxjs/add/operator/mergeMap");
require("rxjs/add/operator/map");
var io = require("socket.io-client");
var createLogger_1 = require("yapreact/utils/createLogger");
var debug = createLogger_1["default"]('socketman:makeSocketEpic');
var createSocketObservable = function (soc) {
    return new Observable_1.Observable(function (observer) {
        soc.on('message', function (message) {
            return observer.next(message);
        });
        soc.on('error', function (message) { return observer.error(message); });
        return function () {
            soc.disconnect();
        };
    });
};
var makeAction = function (type) { return function (m) {
    debug('Receiving', m);
    if (m && m.statusCode !== 200) {
        return { type: "@@socketman/failure/" + type, payload: m };
    }
    if (type === 'UNKNOWN_EVENT') {
        return { type: '@@socketman/UNKNOWN_EVENT', payload: m };
    }
    // `message` data usually has {client,server} keys, but sometimes it is simply an object,
    // or even a string. Which is stupid.
    var data = m.data && m.data.server ? m.data.server : m.data;
    if (m.type !== 'connection' && data && typeof data === 'string') {
        try {
            data = JSON.parse(data);
        }
        catch (err) {
            /* eslint-disable no-console */
            debug('Was trying to parse: ', data);
            debug('Error while parsing incoming socket message', err);
            /* eslint-enable */
            data = {};
        }
    }
    return { type: "@@socketman/" + type, payload: data };
}; };
var socketActions = {
    newMessage: makeAction('NEW_MESSAGE'),
    newAnonymousMessage: makeAction('NEW_ANONYMOUS_MESSAGE'),
    newAnonymousMessageReply: makeAction('NEW_ANONYMOUS_MESSAGE_REPLY'),
    onlineStatus: makeAction('ONLINE_STATUS'),
    pastMessages: makeAction('PAST_MESSAGES'),
    ackSentMessage: makeAction('ACK_SENT_MESSAGE'),
    login: makeAction('LOGIN'),
    updateMerchant: makeAction('UPDATE_MERCHANT'),
    unknown: makeAction('UNKNOWN_EVENT'),
    connected: makeAction('CONNECTED'),
    failedToConnect: makeAction('FAILED_TO_CONNECT'),
    getAnonymousMessages: makeAction('ANONYMOUS_MESSAGES'),
    getAnonymousMessagesV1: makeAction('ANONYMOUS_MESSAGES_V1'),
    getAnonymousMessage: makeAction('ANONYMOUS_MESSAGE'),
    getAnonymousMessageV1: makeAction('ANONYMOUS_MESSAGE_V1'),
    getAnonymousMessageReplies: makeAction('ANONYMOUS_MESSAGE_REPLIES'),
    getAnonymousMessageRepliesV1: makeAction('ANONYMOUS_MESSAGE_REPLIES_V1'),
    sendAnonymousMessageRepliesAck: makeAction('ACK_ANONYMOUS_MESSAGE_REPLY'),
    getAnonymousMessagesDepartment: makeAction('ANONYMOUS_MESSAGES_DEPARTMENT'),
    qrcode: makeAction('WEB_LOGIN_QRCODE'),
    qrsuccess: makeAction('WEB_LOGIN_SUCCESS')
};
var mapSocketToActions = function (message) {
    switch (message.type) {
        case 'connection':
            if (message.statusCode !== 200) {
                return socketActions.failedToConnect(message);
            }
            return socketActions.connected(message);
        case 'getWebMessages':
            return socketActions.pastMessages(message);
        case 'newMessageAck':
        case 'changeMessageStatus_1':
            return socketActions.ackSentMessage(message);
        default:
            return (socketActions[message.type] || socketActions.unknown)(message);
    }
};
var filterAction = function (action, actionType) {
    if (Array.isArray(actionType)) {
        return actionType.indexOf(action.type) >= 0;
    }
    return action.type === actionType;
};
exports["default"] = (function (_a) {
    var socketUrl = _a.socketUrl, startActionType = _a.startActionType, stopActionType = _a.stopActionType, onInit = _a.onInit, actionMapper = _a.actionMapper;
    return function (action$, storeLike) {
        return action$
            .filter(function (action) { return (!exports.socket || !exports.socket.connected) && filterAction(action, startActionType); })
            .mergeMap(function (action) {
            exports.socket = io(socketUrl);
            if (onInit && typeof onInit === 'function') {
                onInit(action, storeLike, exports.socket);
            }
            return createSocketObservable(exports.socket).takeUntil(action$.filter(function (ac) { return filterAction(ac, stopActionType); }));
        })
            .map(function (m) {
            return m.message
                ? m.message
                : m;
        })
            .map(function (m) { return (actionMapper || (function () { return null; }))(m) || mapSocketToActions(m); });
    };
});
//# sourceMappingURL=makeSocketEpic.js.map